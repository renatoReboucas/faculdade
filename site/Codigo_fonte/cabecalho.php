<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Casa da Criança e Adolescente Santo Amaro</title>
    <link rel="stylesheet"  type="text/css" href="node_modules/mdbootstrap/css/mdb.min.css">
    <link rel="stylesheet"  type="text/css" href="node_modules/bootstrap/dist/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Mono" rel="stylesheet">
    <link rel="stylesheet"  type="text/css" href="css/home.css">
    <link rel="stylesheet"  type="text/css" href="css/style.css">
    <link href="css/footer.css" rel="stylesheet" />
</head>
<body>
