<div class="row">
  <div class="col-xs-12 col-md-12 col-sm-12">
    <div class="jarallax">
      <div id="parallax-projects" class="jarallax-img" >
        <br><br>
        <h1 id="projetos" class="white-text">PROJETOS</h1>
      </div>
    </div>
  </div>
</div>
<br>
<div class="row">

  <div class="col-xs-12 col-md-6 col-sm-6">
    <div class="view overlay zoom">
      <img src="images/projetos/img1.jpg" class="img-fluid imgT zoom" alt="img1">
    </div>
  </div>

  <div class="col-xs-12 col-md-6 col-sm-6">

    <p> <strong>O Centro da Criança e do Adolescente - CCA/SEMEAR</strong>
      atendemos uma média de 70 crianças e adolescentes (06 a 14 anos) por ano em situação de vulnerabilidade e risco, contribuindo na sua proteção integral em conjunto com a família e Estado, promovendo o desenvolvimento de suas potencialidades, a partir de ações sócio educativas.
     </p>

  </div>

</div>
<br>
<div class="row">

  <div class="col-xs-12 col-md-6 col-sm-6">
    <h3>ATIVIDADES</h3>
    <p>A Casa da Criança e do Adolescente oferece atividades como: </p>
    <ul class="list-group list-group-flush">
  <li class="list-group-item">Esportes e Recreação</li>
  <li class="list-group-item">Cidadania, Valores, Direitos e Deveres</li>
  <li class="list-group-item">Ensino de Informática</li>
  <li class="list-group-item">Atendimento Psicológico</li>
  <li class="list-group-item">Sensibilização Ambiental</li>
  <li class="list-group-item">Passeios Culturais</li>
  <li class="list-group-item">Ensino de Música</li>
  <li class="list-group-item">Aulas de Inglês</li>
</ul>
  </div>

  <div class="col-xs-12 col-md-6 col-sm-6">
    <div class="view overlay zoom">
      <img src="images/projetos/img2.jpg" class="img-fluid imgT" alt="img1">
    </div>
  </div>

</div>
