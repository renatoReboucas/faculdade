
<br>
<div class="conteiner">
  <div class="row">
    <div class="col-xs-12 col-md-12 col-sm-12">
      <div class="card indigo text-center z-depth-2">
        <!-- <div class="view">
        <img src="images/cards/quem-somos-home.jpg" class="card-img-top" alt="photo">
        <a href="#">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div> -->
    <div class="card-body ">
      <h5 class="card-title white-text"> <strong>Quem Somos</strong> </h5>
      <p class="card-text white-text">
        Somos uma entidade sem fins lucrativos que atende crianças de 0 a 18 anos.
        Apoiamos o processo de desenvolvimento e formação de crianças e adolescentes atendidos em nossos três principais projetos – Casa Lar (Serviços de Acolhimento de Crianças e Adolescentes), Saica (Serviço de Acolhimento Institucional de Crianças e Adolescentes) e CCA (Contra Turno Escolar).
        Para isso, procuramos oferecer um ambiente acolhedor, baseado em respeito e carinho, que possibilite o desenvolvimento integral das crianças e adolescentes atendidos na Entidade.
      </p>
      <!-- <a href="#" class="btn btn-outline-white btn-md waves-effect">Leia mais</a> -->
    </div>
  </div>
</div>
</div>
<br>
<div class="row">
  <div class="col-xs-12 col-md-4 col-sm-6">
    <div class="card mdb-color text-center z-depth-2">
      <h3 class="text-uppercase font-weight-bold cyan-text mt-2 mb-3"><strong>Seja voluntário</strong></h3>
      <div class="view view-cascade overlay">
       <img src="images/cards/quem-somos-home.jpg" class="card-img-top"
         alt="wider">
       <a href="#!">
         <div class="mask rgba-white-slight"></div>
       </a>
     </div>
      <div class="card-body">
        <p class="white-text mb-0">O CCA depende de voluntários para suas atividades. Caso tenha interesse envie o seu contato com sua área de experiencia <a href="https://goo.gl/forms/ernQZhkGZHDDTde13" class="find-more">aqui</a>. Ou ligue para 5686-3288 às 6as feiras das 8:00 as 12:00. </p>
        </div>
      </div>

    </div>
<br>
    <div class="col-xs-12 col-lg-4 col-md-4 mb-4">

      <div class="card red darken-4 text-center z-depth-2">

        <div class="card-body">
          <h3 class="text-uppercase font-weight-bold light-green-text mt-2 mb-3"><strong>Como Ajudar</strong></h3>
          <p class="white-text mb-0">Existem diversas formas de ajudar! </p>
          <p class="white-text mb-0"> Aceitamos doações para apoiar os nossos projetos, bem como doação de materiais (roupas, produtos de limpeza, alimentos, pedagógicos). </p>
          </div>
        </div>

      </div>
<br>
      <div class="col-xs-12 col-lg-4 col-md-4 mb-4">

        <div class="card light-blue darken-1 text-center z-depth-2">
          <h3 class="text-uppercase font-weight-bold purple-text mt-2 mb-3"><strong>Projetos</strong></h3>
          <div class="view view-cascade overlay">
           <img src="images/cards/como-ajudar-home.jpg" class="card-img-top"
             alt="wider">
           <a href="#!">
             <div class="mask rgba-white-slight"></div>
           </a>
         </div>
          <div class="card-body">
            <p class="white-text mb-0"> O Centro da Criança e do Adolescente - CCA atende 70 crianças e adolescentes ( 06 a 14 anos ) separadas em duas salas de aula por período e conta com educadores de diferentes linguagens </p>
            </div>
          </div>

        </div>

      </div>
    </div>
