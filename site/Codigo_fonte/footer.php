
        <?php include("doacaoModal.php"); ?>
        <?php include("comoChegar.php"); ?>
         <div class=" footer">
           <div class="row justify-content-center">
             <div class="col-xs-12 col-md-4 col-sm-4 marge">
                <ul>
                <li><strong>Casa da Criança e Adolescente Santo Amaro Grossarl</strong></li>
                 <li>Rua Padre Chico, 320</li>
                 <li>Santo Amaro - São Paulo - SP / Cep: 04747-090</li>
                 <li>Email: casadacriancacca@hotmail.com</li>
                 <li>Tel: 55 (11)5686-3288 / 3624-2660</li>
                 <li>Fax: 55 (11)5686-3288</li>
                 <li><button type="button" class="btn btn-warning btn-circle" data-toggle="modal" data-target="#fullHeightModalRight" >Como Chegar</button></li>
                 <li><button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#doacao" >FAÇA SUA DOAÇÃO AQUI</button></li>
                </ul>

             </div>
             <div class="col-xs-12 col-md-4 col-sm-4 marge">
                <ul>
                    <li><strong>Registros Institucionais:</strong></li>
                    <li>- Utilidade Pública Federal Portaria nº1026 de 09/01/01 Publ. DOE 12/11/01</li>
                    <li> - Utilidade Pública Federal Portaria n44.732 de 29/02/00 Publ. DOE 01/03/00</li>
                </ul>
             </div>
             <div class="col-xs-12 col-md-4 col-sm-4 marge">
                 <ul>
                     <li><strong>Siga-nos!</strong></li>
                     <li><a href="#"> <div class="sprit-fc"></div> </a></li>
                     <li><a href="https://twitter.com/casadacriancasa"> <div class="sprit-tw"></div> </a></li>
                     <li><a href="http://www.flickr.com/photos/casadacrianca/"> <div class="sprit-fl"></div> </a></li>
                     <li><a href="#"> <div class="sprit-lk"></div> </a></li>


                 </ul>
             </div>
           </div>
         </div>
         <div class="row footer-copyright footer justify-content-center"  align="center">
               <div class="col-xs-12 col-md-6 col-sm-6">
                   © 2018 Copyright
               </div>
           </div>
