
<div class="col-xs-12 col-md-12 col-sm-12">

  <!-- Modal -->
  <div class="modal fade" id="doacao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Doações</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

         <div class="container">
              <div class="row">
              <strong>Escolha uma opção e colabore você também!</strong>
          </div> <br>

          <div class="row">
              <strong>1 - SEJA PADRINHO DE UMA CRIANÇA </strong> <a href="#"> cadastre-se. </a>
          </div> <br>

          <div class="row">
               <strong>2 - PELO SITE:</strong>
          </div> <br>

          <div class="row">
              <p>INICIO FORMULARIO BOTAO PAGSEGURO</p>
          </div> <br>
          <div class="row">
              <form target="pagseguro" action="https://pagseguro.uol.com.br/checkout/doacao.jhtml" method="post">
                  <input type="hidden" name="email_cobranca" value="financeiro@casadacriancasantoamaro.org.br">
                  <input type="hidden" name="moeda" value="BRL">
                  <input type="hidden" src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/doacoes/120x53-doar.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!">
                  <button type="submit" class="btn btn-success btn-circle">Doar</button>
              </form>
          </div> <br>

          <div class="row">
              <strong>3 - EM DEPÓSITO:</strong>
              <br>
              <p>
              BANCO ITAÚ<br>
              AGÊNCIA: 0160 | C.C.: 66.490-0<br>
              CNPJ:  61.054.698/0001-12
              </p>
          </div> <br>

          <div class="row">
              <strong>4 - DOAÇÕES DE NOVOS E USADOS PARA O BAZAR </strong>
          </div> <br>

          <div class="row">
              <strong>5 - DOAÇÃO DE MATERIAIS</strong><br>
          <ul>
              <li>Alimentos perecíveis;</li>
              <li>Verduras, legumes, frutas e polpa de frutas;</li>
              <li> Produtos de confeitaria e padaria;</li>
              <li>Produtos de Higiene Pessoal;</li>
              <li>Produtos de Limpeza;</li>
              <li>Materiais escolares e de escritório;</li>
              <li>Roupas e brinquedos.</li>
          </ul>
          </div>

          <div class="row">
              <strong>6 - APOIO FINANCEIRO E/OU MATERIAL PARA REFORMA DE UMA DE NOSSAS UNIDADES</strong><br>
              <p>Estamos buscando apoio para adequar as instalações da nossa unidade <b>Casa Lar</b>
                 visando um melhor atendimento para os acolhidos.
                 Caso queira colaborar entre contato nos telefones: <br> (11)5686-3288 ou (11)3624-2660.</p>
          </div>
          <div class="row">
              <strong>7 - ATRAVÉS DO FUMCAD</strong><br>
              <p>
                  Seja um patrocinador do projeto "Semeando a Paz", direcionando parte do seu imposto
                   de renda devido e beneficie mais de 100 crianças.<br\>
                  Saiba mais clicando <a href="/images/pdf/CDC_Fumcad_doacao.pdf" target="_blank" class="find-more">aqui</a>.<br>
              <!--  falta arrumar chamada pdf -->
              </p>
          </div>

         </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Sair</button>
        </div>
      </div>
    </div>
  </div>


</div>
