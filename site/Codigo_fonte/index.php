<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Casa da Criança e Adolescente Santo Amaro</title>
    <?php include("linksCss.php"); ?>
</head>
<body>

<?php include("menu.php"); ?>
<br>
<div class="container cent-container">
  <div data-spy="scroll" data-target="#navbar-example2" data-offset="0">

  <?php include("carosel.php"); ?>
  <?php include("cards.php"); ?>
  <br>
  <?php include("projetos.php"); ?>
  <!-- <?php include("fotos.php"); ?> -->
  <?php include("eventos.php"); ?>
  <?php include("formEmail.php"); ?>


<br>
</div>
</div>
<?php include("footer.php"); ?>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/mdbootstrap/js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
