<nav id="navbar-example2" class="navbar navbar-expand-lg navbar-light bg-light navbar-dark ">
   <!-- <a class="navbar-brand" href="#">Navbar</a>  -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#carousel-example-1z">Home <span class="sr-only">(current)</span></a>

    </li>
      <li class="nav-item">
        <a class="nav-link" href="#projetos">Projetos</a>
      </li>

      <!-- <li class="nav-item">
        <a class="nav-link" href="#">Doações</a>
      </li> -->

      <!-- <li class="nav-item">
        <a class="nav-link" href="#eventos">Eventos</a>
      </li> -->

      <li class="nav-item">
        <a class="nav-link" href="#fotos">Fotos</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="#eventos">EVENTOS / PARCEIROS</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="#contato">Contato</a>
      </li>

    </ul>
  </div>
</nav>
